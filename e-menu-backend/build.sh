#To build the project you must configure Maven in your Environment variables
echo 'Clean  project... (require Maven)'
mvn package

echo 'Build project... (require Maven)'
mvn package

#To build docker you require to have installed in your local
echo 'Building docker...'
docker build -t imenu-server:core .

echo 'Publishing on docker hub...'
docker tag imenu-server:core imenu/server:core
#To public your docker image, first you need to login
#docker push imenu/server:core
