##Example used
Video:
- https://www.youtube.com/watch?v=O_XL9oQ1_To&ab_channel=ArjunCodes
- Time: **16:41**
- Time 22:49 start NodeJS

More details
- https://medium.com/bb-tutorials-and-thoughts/how-to-develop-and-build-react-app-with-java-backend-c1e6c5c93ae#:~:text=One%20way%20is%20to%20build%20React%20with%20Java.,it%20with%20the%20java%20code.
- https://www.baeldung.com/spring-security-login-react

React + Boostrap Studio
- https://dev.to/thorstenhirsch/building-react-components-with-bootstrap-studio-a19

##Build Docker image
We have created a build script file, you can run it using the file *build.bat*
```
...
docker build -t imenu-server:core .
docker tag imenu-server:core imenu/server:core
docker push imenu/server:core
```
###Docker-Hub credentials

|Key   |Value   |
|---|---|
| User  | imenu  |
| Email  | imenu@itexp.cloud  |
| Pass  | Consolidacion2  |
Commands:
```
docker login
user:...
pass:...
```
