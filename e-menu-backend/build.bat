@ECHO OFF
::To build the project you must configure Maven in your Environment variables
ECHO Clean  project... (require Maven)
call mvn clean

ECHO Build project... (require Maven)
call mvn package

::To build docker you require to have installed in your local
ECHO Building docker...
docker build -t imenu-server:core .

ECHO Publishing on docker hub...
docker tag imenu-server:core imenu/server:core
::To public your docker image, first you need to login
::docker push imenu/server:core
