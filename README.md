###Run docker compose
Run default file docker compose
```
docker-compose up -d
```

Run specific file
```
docker-compose -f <FILE_NAME> up -d
```

###Application status
Deployed on Hetzner server under docker-compose
- Front: https://imenu.itexp.cloud/
- Front example: https://imenu.itexp.cloud/template/menu.html
- Back: https://imenu-core.itexp.cloud/student/getAll
- DB: http://imenu-db.itexp.cloud/