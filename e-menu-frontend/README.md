## Create
```angular2html
npm init
```

##Start
```angular2html
npm start
```

##Install dependencies
```angular2html
npm install --save express
```


##Build Docker image
We have created a build script file, you can run it using the file *build.bat*
```
...
docker build -t imenu-server:front .
docker tag imenu-server:core imenu/server:front
docker push imenu/server:front
```
###Docker-Hub credentials

|Key   |Value   |
|---|---|
| User  | imenu  |
| Email  | imenu@itexp.cloud  |
| Pass  | Consolidacion2  |
Commands:
```
docker login
user:...
pass:...
```

###Run docker compose
Run default file docker compose
```
docker-compose up -d
```
