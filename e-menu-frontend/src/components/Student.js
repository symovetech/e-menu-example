import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import {Container, Paper, Table, TableBody, TableCell, TableHead, TableRow} from "@mui/material";
import {useEffect, useState} from "react";
import Button from "@mui/material/Button";

export default function Student() {
    const paperStyle = {padding: '50px 20px', width: 600, margin: "20px auto"}
    const[name,setName]=useState('');
    const[address,setAddress]=useState('');
    const[students,setStudents]=useState([]);

    //const classes = useStyles();
    const submitHandleClick = (e) => {
        e.preventDefault();
        const student={name,address};
        console.log(student);
        //fetch("http://localhost:8080/student/add", {
        fetch("https://imenu-core.itexp.cloud/student/add", {
            method:"POST",
            headers:{"Content-Type":"application/json"},
            body:JSON.stringify(student)
        }).then(()=>{
            console.log("New Student added")
        });
    }

    useEffect(()=>{
        //fetch("http://localhost:8080/student/getAll")
        fetch("https://imenu-core.itexp.cloud/student/getAll")
        .then(res=>res.json())
        .then((result)=>{
            setStudents(result);
        })
    },[]);

    return (
        <Container>
            <Paper elevation={2} style={paperStyle}>
                <h1>Add student</h1>
                <p>Static template example: <a href="/template/menu.html" target="_blank">Burger Template</a></p>
                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': {m: 1},
                    }}
                    noValidate
                    autoComplete="off"
                >
                    <TextField id="outlined-basic" label="Student Name" variant="outlined" fullWidth
                    value={name}
                    onChange={(e) => setName(e.target.value)}/>
                    <TextField id="outlined-basic" label="Student Address" variant="outlined" fullWidth
                    value={address}
                    onChange={(e)=>setAddress(e.target.value)}/>

                    <Button variant="contained" onClick={submitHandleClick}>Submit</Button>
                </Box>
                {name} - {address}

            </Paper>

            <Paper elevation={2} style={paperStyle}>
                <h1>Students</h1>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>#</TableCell>
                            <TableCell>Name</TableCell>
                            <TableCell>Address</TableCell>
                        </TableRow>
                    </TableHead>
                    {students.map(student=>(
                        <TableBody>
                            <TableRow
                              key={student.id}
                              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">{student.id}</TableCell>
                                <TableCell>{student.name}</TableCell>
                                <TableCell>{student.address}</TableCell>
                            </TableRow>
                        </TableBody>
                    ))}
                </Table>
            </Paper>

        </Container>
    );
}
